import { writable } from 'svelte/store';

const USER_KEY = 'CALCETES_USER';

let __set = null;

function setUser(user) {
    if (user)
        localStorage.setItem(USER_KEY, JSON.stringify(user));
    else
        localStorage.removeItem(USER_KEY);
    __set(user);
}

function refreshUser(token, set) {
    var user = getUser();
    if (!user)
        return;
    user.Token = token;
    setUser(user);
}

function getUser() {
    var user = localStorage.getItem(USER_KEY);
    if (user)
        return JSON.parse(user);
    return null;
}

function userStore() {
    const { subscribe, set } = writable(getUser());

    __set = set;

    return {
        subscribe,
        loginUser: (arg) => setUser(arg),
        refreshToken: (token) => refreshUser(token),
    }
}

export const logedUser = userStore();