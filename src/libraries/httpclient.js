import { currentUser } from './../libraries/currentUser'
import { server } from './server';
import { logedUser } from './../stores/logedUserStore';

function getHeaders(token) {
    let header = { 'Content-Type': 'application/json' };
    if (token)
        header.CALCETES_IDENTITY = token;
    return header;
}

function request(method, url, object, retry = true){
    return fetch(url, {
        method: method,
        headers: getHeaders(currentUser.Get() ? currentUser.Get().Token : null),
        body: object ? JSON.stringify(object) : null
    })
    .then(async response => {
        let result = await response.json();
        if (response.status == 200) {
            return result;
        }
        else {
            throw result;
        }
    })
    .catch(async error => {
        if (retry && currentUser.Get() && error.Type == "UnauthorizedAccessException") {
            return await retryRefresingToken(error, method, url, object);
        }
        throw error;
    });
}

function retryRefresingToken(error, method, url, object) {
    return fetch(`${ server.identity }UserSessions?userToken=${ currentUser.Get().Token }`, {
        method: 'GET',
        headers: getHeaders()
    })
    .then(async response => {
        let result = await response.json();
        if (response.status == 200) {
            logedUser.refreshToken(result.token);
            return request(method, url, object, false);
        }
        else {
            logedUser.loginUser(null);
            alert('Session expired');
            throw error;
        }
    })
}

function httpClientFactory() {
    return {
        Get: (url) => request('GET', url, null),
        Post: (url, object) => request('POST', url, object),
        Put: (url, object) => request('PUT', url, object),
        Delete: (url) => request('DELETE', url, null)
    }
}

export const httpClient = httpClientFactory();