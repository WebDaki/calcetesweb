let user = null;

export const currentUser = {
    Get: () => user,
    Set: (newUser) => user = newUser
};