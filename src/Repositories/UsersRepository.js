import { server } from './../libraries/server';
import { httpClient } from './../libraries/httpClient';

function usersRepositoryFactory() {
    return {

        GetMe: () => httpClient.Get(
            `${ server.identity }Users/CurrentUser`
        ),

        GetById: (userId) => httpClient.Get(
            `${ server.identity }Users/ById?userId=${ userId }`
        )
        
    }
}

export const usersRepository = usersRepositoryFactory();