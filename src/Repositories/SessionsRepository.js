import { server } from '../libraries/server';
import { httpClient } from '../libraries/httpClient';

function sessionsRepositoryFactory() {
    return {
        Login: (name, pass, userType) => httpClient.Post(
            `${ server.identity }UserSessions`,
            { "Name": name, "Pass": pass, "Type": userType }
        )
    }
}

export const sessionsRepository = sessionsRepositoryFactory();